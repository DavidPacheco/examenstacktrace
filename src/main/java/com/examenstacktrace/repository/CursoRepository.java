package com.examenstacktrace.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examenstacktrace.model.Curso;
import com.examenstacktrace.model.Persona;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long>{
	@Query("SELECT a FROM Persona a JOIN Cursada cu ON cu.alumno = a JOIN Curso c ON c = cu.curso AND c.id = ?1")
	Set<Persona> findAlumnosCurso(Long cursoId);

	@Query("SELECT c FROM  Curso c JOIN Cursada cu ON cu.curso = c AND cu.nota IS NULL JOIN Persona a ON a = cu.alumno AND a.id = ?1")
	Set<Curso> findCursosSinNotaAlumno(Long alumnoId);
	
	@Query("SELECT c FROM Curso c JOIN Persona p ON c.profesor = p AND p.id = ?1")
	Set<Curso> findCursosProfesor(Long profesorId);

	@Query("SELECT a FROM Persona a JOIN Cursada cu ON cu.alumno = a JOIN Curso c ON c = cu.curso AND cu.nota >= c.notaAprobacion AND c.id = ?1")
	Set<Persona> findAlumnosAprobadosCurso(Long cursoId);

	@Query("SELECT c FROM  Curso c JOIN Cursada cu ON cu.curso = c AND cu.nota >= c.notaAprobacion JOIN Persona a ON a = cu.alumno AND a.id = ?1")
	Set<Curso> findCursosAprobadosAlumno(Long alumnoId);
}