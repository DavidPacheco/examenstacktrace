package com.examenstacktrace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cursadas")
public class Cursada {
	private long id;
	private double nota;
    private Persona alumno;
    private Curso curso;

	public Cursada() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "nota")
	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

    @ManyToOne
    @JoinColumn(name="alumno_id", nullable=false)
	public Persona getAlumno() {
		return alumno;
	}

	public void setAlumno(Persona alumno) {
		this.alumno = alumno;
	}

    @ManyToOne
    @JoinColumn(name="curso_id", nullable=false)
	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
}
