package com.examenstacktrace.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examenstacktrace.exception.ResourceNotFoundException;
import com.examenstacktrace.model.Cursada;
import com.examenstacktrace.repository.CursadaRepository;

@RestController
@RequestMapping("/api")
public class CursadaController {
    @Autowired
    private CursadaRepository cursadaRepository;

    @GetMapping("/cursadas")
    public List<Cursada> getAllCursadas() {
        return cursadaRepository.findAll();
    }

    @GetMapping("/cursadas/{id}")
    public ResponseEntity<Cursada> getCursadaById(@PathVariable(value = "id") Long cursadaId)
        throws ResourceNotFoundException {
    	Cursada cursada = cursadaRepository.findById(cursadaId)
          .orElseThrow(() -> new ResourceNotFoundException("Cursada not found for this id :: " + cursadaId));
        return ResponseEntity.ok().body(cursada);
    }
    
    @PostMapping("/cursadas")
    public Cursada createCursada(@Valid @RequestBody Cursada cursada) {
        return cursadaRepository.save(cursada);
    }

    @PutMapping("/cursadas/{id}")
    public ResponseEntity<Cursada> updateCursada(@PathVariable(value = "id") Long cursadaId,
         @Valid @RequestBody Cursada cursadaDetails) throws ResourceNotFoundException {
    	Cursada cursada = cursadaRepository.findById(cursadaId)
        .orElseThrow(() -> new ResourceNotFoundException("Cursada not found for this id :: " + cursadaId));

        cursada.setAlumno(cursadaDetails.getAlumno());
        cursada.setCurso(cursadaDetails.getCurso());
        cursada.setNota(cursadaDetails.getNota());

        final Cursada updatedCursada = cursadaRepository.save(cursada);
        return ResponseEntity.ok(updatedCursada);
    }

    @DeleteMapping("/cursadas/{id}")
    public Map<String, Boolean> deleteCursada(@PathVariable(value = "id") Long cursadaId)
         throws ResourceNotFoundException {
    	Cursada cursada = cursadaRepository.findById(cursadaId)
       .orElseThrow(() -> new ResourceNotFoundException("Cursada not found for this id :: " + cursadaId));

        cursadaRepository.delete(cursada);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}