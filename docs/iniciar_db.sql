USE [Tests]
GO
/****** Object:  Table [dbo].[Personas]    Script Date: 08/11/2019 23:53:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Personas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NULL,
	[tipo_documento] [varchar](10) NULL,
	[nro_documento] [varchar](8) NULL,
	[fecha_nacimiento] [date] NULL,
	[domicilio] [varchar](100) NULL,
	[sexo] [varchar](20) NULL,
	[telefono] [varchar](20) NULL,
	[profesor] [bit] NULL,
 CONSTRAINT [PK_PERSONAS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Personas] ON
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (1, N'Ariel', N'Mitre', N'DNI', N'30000010', CAST(0x7E070B00 AS Date), N'domicilio uno', N'MASCULINO', N'2664-300001', 1)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (2, N'Carmen', N'Velasquez', N'DNI', N'30000011', CAST(0x07240B00 AS Date), N'domicilio dos', N'FEMENINO', N'2664-300002', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (3, N'Juan', N'Rodriguez', N'DNI', N'30000000', CAST(0x7E070B00 AS Date), N'domicilio tres', N'MASCULINO', N'2664-300001', 1)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (4, N'José', N'Huilen', N'DNI', N'30000001', CAST(0x7E070B00 AS Date), N'domicilio cuatro', N'MASCULINO', N'2664-300465', 1)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (5, N'María', N'Tanaño', N'DNI', N'30000002', CAST(0x7E070B00 AS Date), N'domicilio cinco', N'FEMENINO', N'2664-300123', 1)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (6, N'Pedro', N'Circe', N'DNI', N'30000003', CAST(0x7E070B00 AS Date), N'domicilio seis', N'MASCULINO', N'2664-300678', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (7, N'Laura', N'Martinez', N'DNI', N'30000004', CAST(0x07240B00 AS Date), N'domicilio siete', N'FEMENINO', N'2664-300153', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (8, N'Tiziano', N'Mora', N'DNI', N'30000005', CAST(0x07240B00 AS Date), N'domicilio ocho', N'MASCULINO', N'2664-300654', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (9, N'Raúl', N'Del Vicente', N'DNI', N'30000006', CAST(0x07240B00 AS Date), N'domicilio nueve', N'MASCULINO', N'2664-300201', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (10, N'Mirta', N'Viñolo', N'DNI', N'30000007', CAST(0x07240B00 AS Date), N'domicilio diez', N'FEMENINO', N'2664-300486', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (11, N'Paolo', N'Gonzalves', N'DNI', N'30000008', CAST(0x07240B00 AS Date), N'domicilio once', N'MASCULINO', N'2664-300752', 0)
INSERT [dbo].[Personas] ([id], [nombre], [apellido], [tipo_documento], [nro_documento], [fecha_nacimiento], [domicilio], [sexo], [telefono], [profesor]) VALUES (12, N'Yanina', N'Suarez', N'DNI', N'30000009', CAST(0x07240B00 AS Date), N'domicilio doce', N'FEMENINO', N'2664-300002', 0)
SET IDENTITY_INSERT [dbo].[Personas] OFF
/****** Object:  Table [dbo].[Cursos]    Script Date: 08/11/2019 23:53:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cursos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[titulo_profesional] [varchar](100) NOT NULL,
	[horas] [decimal](5, 2) NOT NULL,
	[nota_aprobacion] [decimal](4, 2) NOT NULL,
	[profesor_id] [int] NULL,
 CONSTRAINT [PK_CURSOS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cursos] ON
INSERT [dbo].[Cursos] ([id], [nombre], [titulo_profesional], [horas], [nota_aprobacion], [profesor_id]) VALUES (1, N'Profesorado En Ciencias de la Computación', N'Profesor Informático', CAST(600.00 AS Decimal(5, 2)), CAST(6.00 AS Decimal(4, 2)), NULL)
INSERT [dbo].[Cursos] ([id], [nombre], [titulo_profesional], [horas], [nota_aprobacion], [profesor_id]) VALUES (2, N'JAVA INICIAL', N'Desarrollador inicial en JAVA', CAST(24.00 AS Decimal(5, 2)), CAST(6.00 AS Decimal(4, 2)), 1)
INSERT [dbo].[Cursos] ([id], [nombre], [titulo_profesional], [horas], [nota_aprobacion], [profesor_id]) VALUES (3, N'C# INICIAL', N'Desarrollador inicial en C#', CAST(31.00 AS Decimal(5, 2)), CAST(7.00 AS Decimal(4, 2)), 5)
INSERT [dbo].[Cursos] ([id], [nombre], [titulo_profesional], [horas], [nota_aprobacion], [profesor_id]) VALUES (4, N'JavaScript INICIAL', N'Desarrollador inicial en JavaScript', CAST(15.00 AS Decimal(5, 2)), CAST(7.00 AS Decimal(4, 2)), 3)
INSERT [dbo].[Cursos] ([id], [nombre], [titulo_profesional], [horas], [nota_aprobacion], [profesor_id]) VALUES (5, N'Patrones de Diseño INICIAL', N'Desarrollador en arquitectura inicial', CAST(84.00 AS Decimal(5, 2)), CAST(8.00 AS Decimal(4, 2)), 4)
INSERT [dbo].[Cursos] ([id], [nombre], [titulo_profesional], [horas], [nota_aprobacion], [profesor_id]) VALUES (6, N'PHP INICIAL', N'Desarrollador inicial en PHP', CAST(16.00 AS Decimal(5, 2)), CAST(4.00 AS Decimal(4, 2)), 3)
SET IDENTITY_INSERT [dbo].[Cursos] OFF
/****** Object:  Table [dbo].[Cursadas]    Script Date: 08/11/2019 23:53:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cursadas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[alumno_id] [int] NOT NULL,
	[curso_id] [int] NOT NULL,
	[nota] [decimal](4, 2) NULL,
 CONSTRAINT [PK_CURSADAS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Cursadas] ON
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (1, 1, 1, CAST(7.40 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (2, 3, 1, CAST(7.80 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (3, 4, 1, CAST(7.60 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (4, 5, 1, CAST(8.40 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (5, 6, 4, CAST(2.40 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (6, 6, 5, CAST(2.40 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (7, 5, 6, CAST(8.20 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (8, 6, 2, CAST(9.50 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (9, 7, 3, CAST(10.00 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (10, 8, 4, NULL)
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (11, 9, 5, CAST(3.10 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (12, 10, 6, NULL)
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (13, 11, 2, CAST(2.20 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (14, 12, 3, CAST(1.30 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (15, 6, 4, CAST(9.40 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (16, 7, 5, NULL)
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (17, 8, 6, CAST(8.50 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (18, 9, 2, CAST(7.60 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (19, 10, 3, NULL)
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (20, 11, 4, CAST(6.70 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (21, 12, 5, CAST(5.80 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (22, 8, 6, NULL)
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (23, 9, 2, CAST(4.90 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (24, 10, 3, CAST(1.80 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (25, 11, 4, NULL)
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (26, 8, 5, CAST(2.70 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (27, 9, 6, CAST(3.60 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (28, 2, 2, CAST(8.00 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (29, 10, 2, CAST(8.20 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (30, 2, 3, CAST(9.60 AS Decimal(4, 2)))
INSERT [dbo].[Cursadas] ([id], [alumno_id], [curso_id], [nota]) VALUES (31, 2, 4, CAST(10.00 AS Decimal(4, 2)))
SET IDENTITY_INSERT [dbo].[Cursadas] OFF
/****** Object:  Default [DF__Personas__tipo_d__3F115E1A]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Personas] ADD  DEFAULT ('DNI') FOR [tipo_documento]
GO
/****** Object:  Default [DF__Personas__profes__40058253]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Personas] ADD  DEFAULT ((0)) FOR [profesor]
GO
/****** Object:  ForeignKey [FK_Cursada_Alumno]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Cursadas]  WITH CHECK ADD  CONSTRAINT [FK_Cursada_Alumno] FOREIGN KEY([alumno_id])
REFERENCES [dbo].[Personas] ([id])
GO
ALTER TABLE [dbo].[Cursadas] CHECK CONSTRAINT [FK_Cursada_Alumno]
GO
/****** Object:  ForeignKey [FKl366ih30lg84lqtljhd0k7cyr]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Cursadas]  WITH CHECK ADD  CONSTRAINT [FKl366ih30lg84lqtljhd0k7cyr] FOREIGN KEY([alumno_id])
REFERENCES [dbo].[Personas] ([id])
GO
ALTER TABLE [dbo].[Cursadas] CHECK CONSTRAINT [FKl366ih30lg84lqtljhd0k7cyr]
GO
/****** Object:  ForeignKey [FKpumn8xgb1oho8g1oqeq13jkrq]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Cursadas]  WITH CHECK ADD  CONSTRAINT [FKpumn8xgb1oho8g1oqeq13jkrq] FOREIGN KEY([curso_id])
REFERENCES [dbo].[Cursos] ([id])
GO
ALTER TABLE [dbo].[Cursadas] CHECK CONSTRAINT [FKpumn8xgb1oho8g1oqeq13jkrq]
GO
/****** Object:  ForeignKey [FK_Curso_Profesor]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Cursos]  WITH CHECK ADD  CONSTRAINT [FK_Curso_Profesor] FOREIGN KEY([profesor_id])
REFERENCES [dbo].[Personas] ([id])
GO
ALTER TABLE [dbo].[Cursos] CHECK CONSTRAINT [FK_Curso_Profesor]
GO
/****** Object:  ForeignKey [FKq3nmbfg4bp1y5th28u4msdyk8]    Script Date: 08/11/2019 23:53:12 ******/
ALTER TABLE [dbo].[Cursos]  WITH CHECK ADD  CONSTRAINT [FKq3nmbfg4bp1y5th28u4msdyk8] FOREIGN KEY([profesor_id])
REFERENCES [dbo].[Personas] ([id])
GO
ALTER TABLE [dbo].[Cursos] CHECK CONSTRAINT [FKq3nmbfg4bp1y5th28u4msdyk8]
GO
