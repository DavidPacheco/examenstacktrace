package com.examenstacktrace.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cursos")
public class Curso {
	private long id;
	private String nombre;
	private String tituloProfesional;
	private double horas;
	private double notaAprobacion;
    private Persona profesor;

	public Curso() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "nombre", nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "titulo_profesional", nullable = false)
	public String getTituloProfesional() {
		return tituloProfesional;
	}

	public void setTituloProfesional(String tituloProfesional) {
		this.tituloProfesional = tituloProfesional;
	}

	@Column(name = "horas", nullable = false)
	public double getHoras() {
		return horas;
	}

	public void setHoras(double horas) {
		this.horas = horas;
	}

	@Column(name = "nota_aprobacion", nullable = false)
	public double getNotaAprobacion() {
		return notaAprobacion;
	}

	public void setNotaAprobacion(double notaAprobacion) {
		this.notaAprobacion = notaAprobacion;
	}

    @ManyToOne
    @JoinColumn(name="profesor_id", nullable=false)
	public Persona getProfesor() {
		return profesor;
	}

	public void setProfesor(Persona profesor) {
		this.profesor = profesor;
	}
}
