package com.examenstacktrace.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examenstacktrace.model.Curso;
import com.examenstacktrace.model.Persona;
import com.examenstacktrace.repository.CursoRepository;
import com.examenstacktrace.repository.PersonaRepository;

@RestController
@RequestMapping("/api")
public class AdministracionController {
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private CursoRepository cursoRepository;

    /**
     * Obtener todas las Personas registradas que no son profesores.
     * 
     * @return personas
     */
    @GetMapping("/administracion/alumnos")
    public Set<Persona> getAlumnos() {
        return personaRepository.findAllAlumnos();
    }

    /**
     * Obtener todas las personas que han concurrido a un Curso.
     * @param cursoId Identificación del Curso.
     * @return personas
     */
    @GetMapping("/administracion/alumnos_curso/{id}")
    public Set<Persona> getAlumnosCurso(@PathVariable(value = "id") Long cursoId) {
        return cursoRepository.findAlumnosCurso(cursoId);
    }

    /**
     * Obtener todos los cursos sin nota (no finalizados) a los que concurre una Persona.
     * @param alumnoId Identificación de la persona como alumno.
     * @return cursos
     */
    @GetMapping("/administracion/cursos_sin_nota_alumno/{id}")
    public Set<Curso> getCursosSinNotaAlumno(@PathVariable(value = "id") Long alumnoId) {
        return cursoRepository.findCursosSinNotaAlumno(alumnoId);
    }

    /**
     * Obtener todas las Personas registradas que son profesores.
     * @return personas
     */
    @GetMapping("/administracion/profesores")
    public Set<Persona> getProfesores() {
        return personaRepository.findAllProfesores();
    }

    /**
     * Obtener todos los Cursos dictados por un profesor.
     * @param profesorId Identificación de la persona como profesor.
     * @return cursos
     */
    @GetMapping("/administracion/cursos_profesor/{id}")
    public Set<Curso> getCursosProfesor(@PathVariable(value = "id") Long profesorId) {
        return cursoRepository.findCursosProfesor(profesorId);
    }

    /**
     * Obtener todos las personas que han aprobado un Curso determinado.
     * @param cursoId Identificación del Curso
     * @return personas
     */
    @GetMapping("/administracion/alumnos_aprobados_curso/{id}")
    public Set<Persona> getAlumnosAprobadosCurso(@PathVariable(value = "id") Long cursoId) {
        return cursoRepository.findAlumnosAprobadosCurso(cursoId);
    }

    /**
     * Obtener todos los cursos aprobados por una persona
     * @param alumnoId Identificación de la persona como alumno.
     * @return cursos
     */
    @GetMapping("/administracion/cursos_aprobados_alumno/{id}")
    public Set<Curso> getCursosAprobadosAlumno(@PathVariable(value = "id") Long alumnoId) {
        return cursoRepository.findCursosAprobadosAlumno(alumnoId);
    }
}