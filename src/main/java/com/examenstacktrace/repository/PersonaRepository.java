package com.examenstacktrace.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.examenstacktrace.model.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long>{
	@Query("SELECT a FROM Persona a WHERE a.profesor = 0")
	Set<Persona> findAllAlumnos();
	
	@Query("SELECT p FROM Persona p WHERE p.profesor = 1")
	Set<Persona> findAllProfesores();
}