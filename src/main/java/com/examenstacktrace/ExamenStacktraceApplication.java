package com.examenstacktrace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenStacktraceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenStacktraceApplication.class, args);
	}

}
