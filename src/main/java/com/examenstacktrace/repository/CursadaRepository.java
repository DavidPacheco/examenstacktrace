package com.examenstacktrace.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examenstacktrace.model.Cursada;

@Repository
public interface CursadaRepository extends JpaRepository<Cursada, Long>{

}